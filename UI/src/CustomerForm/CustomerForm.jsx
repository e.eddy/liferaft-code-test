import React from "react";
import { withStyles } from "@material-ui/core/styles";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";
import Snackbar from "@mui/material/Snackbar";
import Typography from "@mui/material/Typography";
import CircularProgress from "@mui/material/CircularProgress";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import Alert from "@mui/material/Alert";
import InputAdornment from "@mui/material/InputAdornment";
import {
  AccountCircle as NameIcon,
  Email as EmailIcon,
  Phone as PhoneIcon,
  Home as StreetIcon,
  Public as CountryIcon,
  Map as StateProvinceIcon,
  LocationOn as CityIcon,
  Numbers as HouseNumberIcon,
} from "@mui/icons-material";
import { countries, states, provinces } from "./locations";
import Axios from "axios";

const useStyles = () => ({
  customerForm: {
    padding: "1rem",
  },
  formBody: {
    padding: "1rem",
  },
  formField: {
    minHeight: "4.5rem",
  },
});

class CustomerForm extends React.Component {
  constructor(props) {
    super(props);
    this.initialCustomerState = {
      customerName: "",
      customerEmail: "",
      customerPhoneNumber: "",
      customerHouseNumber: "",
      customerStreetName: "",
      customerCity: "",
      customerStateProvince: "",
      customerCountry: "CA", // default to Canada
    };

    this.state = {
      ...this.initialCustomerState,
      snackbarShown: false,
      snackbarState: "",
      snackbarMessage: "",
      formErrors: {},
      loading: false,
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSnackbarClose = this.handleSnackbarClose.bind(this);
  }

  handleInputChange(event) {
    const value = event.target.value;
    const name = event.target.name;

    // Reset state/province value when switching between dropdown menus
    if (
      name === "customerCountry" &&
      (this.state.customerCountry === "CA" ||
        this.state.customerCountry === "US" ||
        value === "CA" ||
        value === "US")
    ) {
      this.setState({ customerStateProvince: "" });
    }
    this.setState({ [name]: value });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ loading: true });

    var customer = {
      name: this.state.customerName,
      email: this.state.customerEmail,
      phoneNumber: this.state.customerPhoneNumber,
      address: {
        houseNumber: this.state.customerHouseNumber,
        streetName: this.state.customerStreetName,
        city: this.state.customerCity,
        stateProvince: this.state.customerStateProvince,
        country: this.state.customerCountry,
      },
    };

    Axios.post("http://localhost:3001/api/addCustomer", {
      customer: customer,
    })
      .then((response) => {
        if (response.data.success === true) {
          this.setState({
            ...this.initialCustomerState,
            formErrors: {},
            snackbarMessage: "Customer addition success",
            snackbarState: "success",
            snackbarShown: true,
            loading: false,
          });
        } else {
          if (response.data.error.type === "formValidation") {
            this.setState({
              snackbarMessage:
                "Please review the fields and enter valid information",
              formErrors: response.data.error,
              snackbarState: "error",
              snackbarShown: true,
              loading: false,
            });
          } else {
            this.setState({
              formErrors: {},
              snackbarMessage: response.data.error,
              snackbarState: "error",
              snackbarShown: true,
              loading: false,
            });
          }
        }
      })
      .catch((err) => {
        this.setState({
          formErrors: {},
          snackbarMessage: "Server is not available",
          snackbarState: "error",
          snackbarShown: true,
          loading: false,
        });
      });
  }

  handleSnackbarClose() {
    this.setState({ snackbarShown: false });
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.customerForm}>
        <Typography variant="h5" component="div">
          New Customer Information
        </Typography>
        <FormControl className={classes.formBody}>
          <TextField
            className={classes.formField}
            name="customerName"
            label="Name:"
            value={this.state.customerName}
            onChange={this.handleInputChange}
            helperText={this.state.formErrors.name}
            error={this.state.formErrors.name ? true : false}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <NameIcon />
                </InputAdornment>
              ),
            }}
          />
          <TextField
            className={classes.formField}
            name="customerEmail"
            label="Email:"
            value={this.state.customerEmail}
            onChange={this.handleInputChange}
            helperText={this.state.formErrors.email}
            error={this.state.formErrors.email ? true : false}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <EmailIcon />
                </InputAdornment>
              ),
            }}
          />
          <TextField
            className={classes.formField}
            name="customerPhoneNumber"
            label="Phone Number (xxx-xxx-xxxx):"
            value={this.state.customerPhoneNumber}
            onChange={this.handleInputChange}
            helperText={this.state.formErrors.phone}
            error={this.state.formErrors.phone ? true : false}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <PhoneIcon />
                </InputAdornment>
              ),
            }}
          />
          <Typography variant="h6" component="div" pt={1}>
            Address
          </Typography>
          <TextField
            className={classes.formField}
            name="customerHouseNumber"
            label="House Number:"
            value={this.state.customerHouseNumber}
            onChange={this.handleInputChange}
            helperText={this.state.formErrors.houseNumber}
            error={this.state.formErrors.houseNumber ? true : false}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <HouseNumberIcon />
                </InputAdornment>
              ),
            }}
          />
          <TextField
            className={classes.formField}
            name="customerStreetName"
            label="Street Name:"
            value={this.state.customerStreetName}
            onChange={this.handleInputChange}
            helperText={this.state.formErrors.streetName}
            error={this.state.formErrors.streetName ? true : false}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <StreetIcon />
                </InputAdornment>
              ),
            }}
          />
          <TextField
            className={classes.formField}
            name="customerCountry"
            select
            label="Country: "
            value={this.state.customerCountry}
            onChange={this.handleInputChange}
            helperText={this.state.formErrors.country}
            error={this.state.formErrors.country ? true : false}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <CountryIcon />
                </InputAdornment>
              ),
            }}
          >
            {countries.map((country) => (
              <MenuItem key={country.code} value={country.code}>
                {country.name}
              </MenuItem>
            ))}
          </TextField>
          <TextField
            className={classes.formField}
            name="customerStateProvince"
            select={
              this.state.customerCountry === "CA" ||
              this.state.customerCountry === "US"
            }
            label="State/Province:"
            value={this.state.customerStateProvince}
            onChange={this.handleInputChange}
            helperText={this.state.formErrors.stateProvince}
            error={this.state.formErrors.stateProvince ? true : false}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <StateProvinceIcon />
                </InputAdornment>
              ),
            }}
          >
            {this.state.customerCountry === "US"
              ? states.map((state) => (
                  <MenuItem key={state.code} value={state.code}>
                    {state.name}
                  </MenuItem>
                ))
              : provinces.map((province) => (
                  <MenuItem key={province.code} value={province.code}>
                    {province.name}
                  </MenuItem>
                ))}
          </TextField>
          <TextField
            className={classes.formField}
            name="customerCity"
            label="City:"
            value={this.state.customerCity}
            onChange={this.handleInputChange}
            helperText={this.state.formErrors.city}
            error={this.state.formErrors.city ? true : false}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <CityIcon />
                </InputAdornment>
              ),
            }}
          />
          <br />
          <Box sx={{ display: "flex", alignItems: "center" }}>
            <Button
              color="secondary"
              variant="contained"
              onClick={this.handleSubmit}
              disabled={this.state.loading}
            >
              Submit
            </Button>
            {this.state.loading && (
              <CircularProgress size={30} sx={{ marginLeft: 2 }} />
            )}
          </Box>
        </FormControl>
        <Snackbar
          open={this.state.snackbarShown}
          autoHideDuration={8000}
          onClose={this.handleSnackbarClose}
        >
          <Alert
            onClose={this.handleSnackbarClose}
            severity={this.state.snackbarState || undefined}
          >
            {this.state.snackbarMessage}
          </Alert>
        </Snackbar>
      </div>
    );
  }
}

export default withStyles(useStyles)(CustomerForm);
