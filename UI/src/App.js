import CustomerForm from "./CustomerForm/CustomerForm";
import Typography from "@mui/material/Typography";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import { ThemeProvider, createTheme } from "@mui/material/styles";

const theme = createTheme({
  palette: {
    primary: {
      main: "#212228", //Gray
    },
    secondary: {
      main: "#fcb040", //Yellow
    },
  },
  components: {
    MuiTextField: {
      defaultProps: {
        variant: "standard",
        margin: "dense",
      },
    },
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <AppBar>
          <Toolbar>
            <Typography variant="h4" component="div">
              LifeRaft
            </Typography>
          </Toolbar>
        </AppBar>
        <Toolbar />
        <CustomerForm />
      </div>
    </ThemeProvider>
  );
}

export default App;
