# LifeRaft Code Test

Simple UI and server to handle the collecion of customer contact information. Collected information is stored in `api/data/customers.txt`

## Steps to deploy with Docker

- Run `docker-compose up` from the `liferaft-code-test` folder
- In a browser, open `http://localhost:3000`

## Steps to deploy locally

- Run the React UI by navigating to the `ui` folder, and running `npm start`
- Run the server by navigating to the `api` folder, and running `npm start`
- In a browser, open `http://localhost:3000`
