const express = require("express");
const fs = require("fs");
const cors = require("cors");
const bodyParser = require("body-parser");

const app = express();
app.use(cors());
app.use(bodyParser.json());

const cusomterDataPath = "./data/customers.txt";

app.listen(3001, () => {
  console.log(`Server listening on 3001`);
});

// Post customer information and validate
app.post("/api/addCustomer", (req, res) => {
  const customer = req.body.customer;
  const validationResult = validateCustomer(customer);

  // if successful validation
  if (validationResult == true) {
    customer.address.houseNumber = parseInt(customer.address.houseNumber);

    // Add new customer to array of existing customers
    var customers = [];
    if (fs.existsSync(cusomterDataPath)) {
      var customersText = fs.readFileSync(cusomterDataPath);

      // If file is empty or not in proper format
      try {
        customers = JSON.parse(customersText);
      } catch {}
    }

    customers.push(customer);

    fs.writeFile(cusomterDataPath, JSON.stringify(customers), (err) => {
      if (err) {
        res.send({ success: false, error: "Error saving data" });
      } else {
        res.send({ success: true });
      }
    });
  } else {
    res.send({
      success: false,
      error: { type: "formValidation", ...validationResult },
    });
  }
});

// Validate customer fields
function validateCustomer(customer) {
  errors = {};

  // Accept first and last name of letters, possibility for middle names
  var validName = /^[a-zA-Z.'-,]+ [a-zA-Z.'-, ]+$/;
  if (!validName.test(customer.name)) {
    errors.name =
      "First and last name required with no numbers or special characters";
  }

  // Accept valid email formats (https://www.codespot.org/javascript-email-validation/)
  var validEmail =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!validEmail.test(customer.email)) {
    errors.email = "Valid email required";
  }

  // Accept phone number format xxx-xxx-xxxx
  var validPhone = /^\d{3}-\d{3}-\d{4}$/;
  if (!validPhone.test(customer.phoneNumber)) {
    errors.phone = "Number in the format xxx-xxx-xxxx required";
  }

  // Accept integer
  var validHouseNumber = /^\d+$/;
  if (!validHouseNumber.test(customer.address.houseNumber)) {
    errors.houseNumber = "Valid number required";
  }

  // Accept string
  var validStreetName = /^[A-Za-z-.' ]+$/;
  if (!validStreetName.test(customer.address.streetName)) {
    errors.streetName = "Valid street name required";
  }

  // Accept string
  var validCity = /^[A-Za-z-.' ]+$/;
  if (!validCity.test(customer.address.city)) {
    errors.city = "Valid city name required";
  }

  // Accept string
  var validStateProvince = /^[A-Za-z-.' ]+$/;
  if (!validStateProvince.test(customer.address.stateProvince)) {
    errors.stateProvince = "Valid state or province required";
  }

  // Accept two letter country codes
  var validCountry = /^[A-Z]{2}$/;
  if (!validCountry.test(customer.address.country)) {
    errors.country = "Calid country is required";
  }

  // if no errors, return true
  return Object.keys(errors).length ? errors : true;
}
